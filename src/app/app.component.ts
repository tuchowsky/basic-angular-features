import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = "my own title";
  name = "Artur";
  age = 34;

  today = new Date();

  items: string[] = [
    'This',
    'Is',
    'Awesome'
  ]

  isOpen = false;

  isGreetingVisible () {
    this.isOpen = !this.isOpen;
  }

  addItemToArray(name) {
    this.items.push(name);
  }  
  
  removeItemFromArray(index) {
    this.items.splice(index, 1);
  }
}
